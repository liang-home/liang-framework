package org.liang.common.core.constant;

/**
 * 服务名称
 * @author LiangL liang_work2020@163.com
 * @date 2021/4/1 10:20 下午
 */
public class ServiceNameConstants {
    /** 认证服务的service id */
    public static final String AUTH_SERVICE = "liang-auth";

    /** 系统模块的service id */
    public static final String SYSTEM_SERVICE = "liang-system";

    /** 文件服务的service id */
    public static final String FILE_SERVICE = "liang-file";
}
