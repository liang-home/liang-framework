package org.liang.common.core.domain;

import lombok.Getter;
import lombok.Setter;
import org.liang.common.core.constant.Constants;

import java.io.Serializable;

/**
 * 响应信息主体
 * @author LiangL liang_work2020@163.com
 * @date 2021/4/1 10:30 下午
 */
@Getter
@Setter
public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 成功 */
    public static final int SUCCESS = Constants.SUCCESS;
    /** 失败 */
    public static final int FAIL = Constants.FAIL;

    /** 状态码 */
    private int code;
    /** 响应消息 */
    private String msg;
    /** 响应数据 */
    private T data;

    public static <T> R<T> ok()
    {
        return restResult(null, SUCCESS, null);
    }
    public static <T> R<T> ok(T data)
    {
        return restResult(data, SUCCESS, null);
    }
    public static <T> R<T> ok(T data, String msg) {
        return restResult(data, SUCCESS, msg);
    }

    public static <T> R<T> fail()
    {
        return restResult(null, FAIL, null);
    }
    public static <T> R<T> fail(String msg)
    {
        return restResult(null, FAIL, msg);
    }

    public static <T> R<T> fail(T data)
    {
        return restResult(data, FAIL, null);
    }
    public static <T> R<T> fail(T data, String msg)
    {
        return restResult(data, FAIL, msg);
    }
    public static <T> R<T> fail(int code, String msg)
    {
        return restResult(null, code, msg);
    }

    private static <T> R<T> restResult(T data, int code, String msg) {
        R<T> apiResult = new R<T>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setMsg(msg);
        return apiResult;
    }
}
